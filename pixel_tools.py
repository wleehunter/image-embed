#!/usr/bin/python
import os
import sys
from PIL import Image

class PixelTools:

  def newImage(self, imageMode, imageSize):
    return Image.new(imageMode, imageSize)

  def openImage(self, filename):
    return Image.open(filename)

  def getPixelList(self, imgName):
    im = Image.open(imgName)
    return list(im.getdata())

  def getLeastSignificant(self, rgb):
    pixelIntVal = self.getIntValFromTuple(rgb)
    pixelBinString = "{0:b}".format(pixelIntVal)
    return pixelBinString[len(pixelBinString) - 1]

  def getIntValFromTuple(self, rgb):
    return rgb[0]* (256 * 256) + rgb[1] * 256 + rgb[2]

  def getEncryptedTuple(self, rgb, leastSignificant):
    pixelIntVal = self.getIntValFromTuple(rgb)
    pixelBinString = "{0:b}".format(pixelIntVal)
    pixelList = list(pixelBinString)
    pixelList[len(pixelBinString) - 1] = leastSignificant
    encryptedBinString = "".join(pixelList)
    return self.getTupleFromBinString(encryptedBinString)

  def getDecryptedTuple(self, leastSignificant):
    if leastSignificant == "1":
      decryptedTuple = (255,255,255)
    elif leastSignificant == "0":
      decryptedTuple = (0,0,0)
    return decryptedTuple

  def getTupleFromBinString(self, binString):
    intVal = int(binString, 2)
    blue = intVal & 255
    green = (intVal >> 8) & 255
    red = (intVal >> 16) & 255
    return (red, green, blue)

#!/usr/bin/python
import sys
import os
from PIL import Image
from pixel_tools import PixelTools

class Decrypter:

  def __init__(self):
    self.pixelTools = PixelTools()

  def decrypt(self, horse):
    self.horseName = horse
    horseImg = self.pixelTools.openImage(self.horseName)
    horsePixels = self.getHorsePixels()
    decryptedList = []
    for idx, t in enumerate(horsePixels):
      leastSignificant = self.pixelTools.getLeastSignificant(t)
      decryptedList.append(self.pixelTools.getDecryptedTuple(leastSignificant))

    decryptedImg = self.pixelTools.newImage(horseImg.mode, horseImg.size)
    decryptedImg.putdata(decryptedList)
    decryptedImg.save("decrypted.png", "PNG")



  def getHorsePixels(self):
    return self.pixelTools.getPixelList(self.horseName)


if __name__ == "__main__":
  decrypter = Decrypter()
  decrypter.decrypt(sys.argv[1])

  

  


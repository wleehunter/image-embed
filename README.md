# README #

This is a steganography experiment that I'm doing in python. It allows you to embed a black and white image into another image and get it back. Still needs some work. There's a bunch of noise in the decrypted image. 

### What is this repository for? ###

* Summary: Embed the data that makes up a black and white image into another image. And extract it back out.
* Version: 0.0.1

### How do I get set up? ###

#### Dependencies ####
Pillow (an image library in Python)  
`pip install Pillow`  
or  
`easy install Pillow`  

Looks like OSX has some additional dependencies. (I haven't tried this)  
`brew install libtiff libjpeg webp littlecms`  
...and then  
`pip install Pillow`  

### Example usage (using the two images in the repo) ###

Encrypt: (this should create an image named encrypted.png)  
`$ python image_encrypter.py cookie.message.png cookie.monster.png`  

Decrypt: (this should create an image named decrypted.png)  
`$python image_decrypter.py encrypted.png`
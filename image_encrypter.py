#!/usr/bin/python
import sys
import os
from PIL import Image
from pixel_tools import PixelTools

class Encryptor:

  def __init__(self):
    self.pixelTools = PixelTools()

  def encrypt(self, trojan, horse):
    self.trojanName = trojan
    self.horseName = horse
    horseImg = self.pixelTools.openImage(self.horseName)
    trojanPixels = self.getTrojanPixels()
    horsePixels = self.getHorsePixels()
    encryptedList = []
    for idx, t in enumerate(trojanPixels):
      leastSignificant = self.pixelTools.getLeastSignificant(t)
      encryptedList.append(self.pixelTools.getEncryptedTuple(horsePixels[idx], leastSignificant))

    encryptedImg = self.pixelTools.newImage(horseImg.mode, horseImg.size)
    encryptedImg.putdata(encryptedList)
    encryptedImg.save("encrypted.png", "PNG")

  def getTrojanPixels(self):
    return self.pixelTools.getPixelList(self.trojanName)

  def getHorsePixels(self):
    return self.pixelTools.getPixelList(self.horseName)


if __name__ == "__main__":
  encryptor = Encryptor()
  encryptor.encrypt(sys.argv[1], sys.argv[2])

  

  

